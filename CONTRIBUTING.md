Copy Files to your local machine

Run  - composer install

Run in terminal - php artisan migrate  (to create DB and tables)

Create virtual host(root directory from /public);

To send email from localhost, you need to configure local SMTP(https://stackoverflow.com/questions/21836282/php-function-mail-isnt-working)

We need your live url for set up in Stripe webhook.

And you need have supervisor module on your server, for Laravel jobs.

Thats all! Thanks
