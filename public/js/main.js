$(document).ready(function(){
    $('#products').change(function(){
       var values = $(this).val();
       $.ajax({
           url:'/getPrices',
           data: {
               values:values
           },
           type:'GET',
           success: function (response) {
               $('span.total').html(response.price);
           }
       })
    });

    $('input[name=discount]').on('input', function () {
        $('span.discount').text('');
    });

    $('button.validate-discount').click(function () {

        var coupon = $('input[name=discount]').val();
        if (coupon){
            $.ajax({
                url: '/validate-discount',
                data: {
                    coupon:coupon
                },
                type: 'GET',
                success: function (response) {
                    if (response.success){
                        console.log(response.coupon);
                       if (response.type === 'percent'){
                           $('span.discount').text('Your coupon is valid and you have discount ' + response.coupon.discountPercent + '%');
                       }
                        if (response.type === 'price'){
                            $('span.discount').text('Your coupon is valid and you have discount $' + response.coupon.discountPrice);
                        }

                    }else {

                        $('span.discount').text('Your coupon is invalid').css('color','red');
                        return false;
                    }
                }
            })
        }else {

            $('span.discount').text('Your coupon is invalid').css('color','red');
            return false;
        }
    });


});
// Create a Stripe client
let stripe = Stripe('pk_test_nbYGckga80dO4DdBDZjJ69mq');

// Create an instance of Elements
let elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
let style = {
    base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

// Create an instance of the card Element
let card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function (event) {
    let displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
});

// Handle form submission
let form = document.getElementById('payment-form');
form.addEventListener('submit', function (event) {

    event.preventDefault();
    $('.loading').css('display', 'block');

    stripe.createToken(card).then(function (result) {
        if (result.error) {
            $('.loading').css('display', 'none');

            // Inform the user if there was an error
            let errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/addCard',
                type: 'post',
                data: {
                    token: result.token.id
                },
                success: function (response) {
                    if (response.success === 'true') {
                        window.location.reload();
                    } else {
                        $('.error').css('display', 'block')
                        $('.loading').css('display', 'none');

                    }
                }
            })

        }
    });
});




$('.delete-button').on('click', function (e) {
    e.preventDefault();
    $('.loading').css('display', 'block');
    let cardId = $(this).attr('data-id');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/deleteCard',
        type: 'post',
        data: {
            cardId: cardId
        },
        success: function (response) {
            if (response.success === 'true') {
                window.location.reload();
            } else {
                $('.loading').css('display', 'none');

                $('.error').css('display', 'block')
            }
        }
    });


});


