<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAccountIdColumnOnStripeAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stripe_accounts', function (Blueprint $table) {
            $table->renameColumn('account_id', 'accountId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stripe_accounts', function (Blueprint $table) {
            $table->renameColumn('accountId', 'account_id');
        });


    }
}