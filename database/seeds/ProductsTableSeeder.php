<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Product 2',
                'price' => 300
            ],
            [
                'name' => 'Product 3',
                'price' => 500

            ],
            [
                'name' => 'Product 4',
                'price' => 1000

            ],
            [
                'name' => 'Product 1',
                'price' => 200
            ]
        ];

        DB::table('products')->insert($data);
    }
}
