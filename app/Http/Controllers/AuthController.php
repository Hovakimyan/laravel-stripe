<?php

namespace App\Http\Controllers;

use App\Http\Helpers\StripeHelper;
use App\StripeAccount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Stripe\Customer;
use Stripe\Stripe;

class AuthController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function login(Request $request)
    {
        if ($request->isMethod('post')){
            $rules = [
                'email' => 'required|email|min:6|max:30',
                'password' => 'required|min:6|max:20',
            ];

            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()){
                return Redirect::back()->withErrors($validator);
            }

            $email = $request->input('email');
            $password = $request->input('password');
            if (Auth::attempt(['email' => $email, 'password' => $password, 'verified' => '1'])) {
                return redirect('/home');
            }
            return Redirect::back()->withErrors('Your inputed data was incorrect or Your user was not verified yet');

        }
        return view('auth.login');
    }

    /**
     * @param Request $request
     * @return User|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request)
    {
        if ($request->isMethod('post')){
            $rules = [
                'fullName'       => 'required|min:3|max:50',
                'email'          => 'required|email|unique:users,email',
                'phoneNumber'    => 'required|min:8|max:12',
                'password'       => 'required|min:6|max:20',
                'repeatPassword' => 'required|min:6|max:20|same:password',
                'role'           => 'required'
            ];

            $validator = Validator::make($request->all(),$rules);

            if ($validator->fails()){
                return Redirect::back()->withErrors($validator);
            }
            $token = self::generateRandomString(40);
            $user = new User([
                'fullName'          => $request->input('fullName'),
                'email'             => $request->input('email'),
                'phoneNumber'       => $request->input('phoneNumber'),
                'password'          =>  bcrypt($request->input('password')),
                'verified'          => '0',
                'registrationToken' => $token,
                'role'              => $request->input('role')
            ]);
            $user->save();
            $data = [
                'name'  => $user->fullName,
                'token' => $user->registrationToken,
                'email' => $user->email
            ];
            if(!self::sendEmail($data)){
                return Redirect::back()->withErrors('Something went wrong! Please try again Later..');
            }

            return Redirect('/')->withErrors('You have successfully registered, please  check your email for confirmation.');
        }
        return view('auth.register');
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 15)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function sendEmail($data = [])
    {
        try{
            Mail::send('mail', $data, function($message) use( &$data) {

                $message->to($data['email'])->subject
                ('Account Verification');
                $message->from('garik.khachanyan@gmail.com','Test Project');
            });
            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verifyAccount(Request $request)
    {
        $token = $request->input('t');
        $user = User::where('registrationToken', $token)
                ->first();
        $user->verified = '1';
        $user->save();
        if ($user->role == User::ROLE_CUSTOMER){
            if(!StripeAccount::createStripeCustomer($user->email, $user->id)){
                return Redirect('/')->withErrors('Something went wrong. Please try again Later');
            };
        }
        if ($user->role == User::ROLE_SERVANT){
            if(!StripeAccount::createAccount($user->id,'US', 'custom')){
                return Redirect('/')->withErrors('Something went wrong. Please try again Later');
            };
        }

        return Redirect('/')->withErrors('You have successfully verified your account! Please Login To Continue');
    }

    /**
     * @param $email
     * @param $userId
     * @return bool
     */



    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}