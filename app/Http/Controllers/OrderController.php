<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Http\Helpers\StripeHelper;
use App\Order;
use App\Product;
use App\StripeAccount;
use App\User;
use App\Jobs\Order as jobOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Stripe\Stripe;

class OrderController extends Controller
{
    const SERVANT_PERCENT = 20;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_API_KEY'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function get($id)
    {
        $role = auth()->user()->role;
        $order = Order::getOrder($id,$role,auth()->user()->id);
        if ($order){
            return view('singleOrder')->with(['order' => $order]);
        }

        return redirect('/orders');

    }

    /**
     * @param Request $request
     * @return $this
     */
    public function orders(Request $request)
    {



        if ($request->isMethod('post')){
            if (self::ifUserCardExist()){
                $rules = [
                    'products' => 'required',
                    'description' => 'max:140'
                ];
                $validator = Validator::make($request->all(),$rules);
                if ($validator->fails()){
                    return Redirect::back()->withErrors($validator);
                }
                $discount = $request->input('discount');
                $discount = Discount::isValidCoupon($discount);
                $amount = Product::getOrderTotal($request->input('products'),$discount);

                $data['amount'] = $amount;
                $data['description'] = $request->input('description');
                jobOrder::dispatch($data)->delay(Carbon::now()->addMinutes(2));
            }else{
                echo 'Customer Card Doesnt exist';
            }


            }
        $orderType = 'customerId';
        if (auth()->user()->role == User::ROLE_SERVANT){
            $orderType = 'servantId';
        }

        $orders = Order::getOrders($orderType);
        $products = Product::all();

        return view('orders')->with(['orders' => $orders, 'products' => $products]);



    }

    /**
     * @return bool
     */

    public static function ifUserCardExist(){
        $customerID = auth()->user()->stripeAccount->customerId;
        $customer = StripeHelper::getCustomer($customerID);
        $cardID = $customer->default_source;

        if(isset($cardID)){
            return true;
        }
        return false;
    }


    /**
     * @param $amount
     * @return array
     */
    public static function calculateAmount($amount)
    {
        $servantAmount = $amount * self::SERVANT_PERCENT / 100;
        $ownerAmount = $amount - $servantAmount;
        return ([
           'servant' => $servantAmount,
           'owner' => $ownerAmount
        ]);
    }

    public function validateDiscount(Request $request)
    {
        $coupon = $request->input('coupon');
        $check = Discount::isValidCoupon($coupon);
        $success = false;
        $type = '';
        if ($check){
            $type = Discount::getCouponType($check);
            $success = true;
        }

        return response()->json(['success'=> $success,'coupon'=> $check, 'type' => $type]);
    }

    /**
     *  Stripe webhook events
     */
    public function webhook()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        switch ($data['type']) {

            case 'charge.succeeded' :
                Order::updateOrderByChargeId($data->object->id);
        }
    }

    public function getPrices(Request $request)
    {
        $values = $request->input('values');
        $price = Product::getOrderTotal($values);
        return response()->json(['price' => $price], 200);

    }
}
