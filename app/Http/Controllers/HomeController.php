<?php

namespace App\Http\Controllers;

use App\Http\Helpers\StripeHelper;
use App\StripeAccount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Stripe;

class HomeController extends Controller
{
    const STRIPE_SECRET_KEY = 'sk_test_3DHZ4mnwhVOZDAxhNIUreVOb';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return view('home');
        }
        if (auth()->user()->role == User::ROLE_CUSTOMER){
            $stripeAccount = self::getCurrentUserStripeAccount();
            Stripe::setApiKey(self::STRIPE_SECRET_KEY);
            $data['customer'] = Customer::retrieve($stripeAccount->customerId)->sources->all(array(
                    'limit'=> 20, 'object' => 'card')
            );
            return view('home', $data);
        }

        return redirect('/orders');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function addCard(Request $request)
    {
        if(!Auth::check()){
            return view('home');
        }
        Stripe::setApiKey(self::STRIPE_SECRET_KEY);
        if(empty($request->input('token'))){
            return response()->json([
                'success' => 'false',
                'message' => 'Token is Empty'
            ]);
        }
        $stripeAccount = self::getCurrentUserStripeAccount();

        try {
            $cu = Customer::retrieve($stripeAccount->customerId); // stored in your application
            $cu->sources->create(array("source" =>$request->input('token')));
            $success = "Your card added successfully!";
            return response()->json([
                'success' => 'true',
                'message' => $success
            ]);
        }catch(Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $error = $err['message'];
            return response()->json([
                'success' => 'false',
                'message' => $error
            ]);
        }
    }

    /**
     * @return mixed
     */
    protected static function getCurrentUserStripeAccount()
    {
        $user = Auth::user();
        return StripeAccount::where('userId',$user->id)->first();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function deleteCard(Request $request)
    {
        if(!Auth::check()){
            return view('home');
        }
        Stripe::setApiKey(self::STRIPE_SECRET_KEY);
        if(empty($request->input('cardId'))){
            echo 'error';
        }
        $stripeAccount = self::getCurrentUserStripeAccount();
        try {
            $cu = Customer::retrieve($stripeAccount->customerId); // stored in your application
            $cu->sources->retrieve($request->input('cardId'))->delete();
            $success = "Your card removed successfully!";
            return response()->json([
                'success' => 'true',
                'message' => $success
            ]);
        }catch(Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $error = $err['message'];
            return response()->json([
                'success' => 'false',
                'message' => $error
            ]);
        }
    }


}
