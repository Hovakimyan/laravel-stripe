<?php

namespace App\Http\Middleware;

use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_route = request()->route()->getName();
        view()->share('route', $current_route);
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}