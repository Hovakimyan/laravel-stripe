<?php

namespace App\Http\Helpers;

use Carbon\Carbon;
use Stripe\Account;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Order;
use Stripe\Product;
use Stripe\Refund;
use Stripe\SKU;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Token;
use Stripe\Transfer;

class StripeHelper
{

    /**
     * Create card token
     *
     * @param $number
     * @param $expMonth
     * @param $expYear
     * @param $cvc
     * @param array $metadata
     * @return Token
     */
    public static function createToken($number, $expMonth, $expYear, $cvc, $metadata = [])
    {
        $token = Token::create(array(
            "card" => array(
                "number" => $number,
                "exp_month" => $expMonth,
                "exp_year" => $expYear,
                "cvc" => $cvc,
                "metadata" => $metadata
            )
        ));

        return $token;
    }

    /**
     * Get stripe customer
     *
     * @param $customerId
     * @return Customer
     */

    public static function getCustomer($customerId)
    {
        $customer = Customer::retrieve($customerId);
        return $customer;
    }

    /**
     * Create stripe customer
     *
     * @param $token
     * @param $email
     * @param $description
     * @return Customer
     */
    public static function createCustomer($token, $email, $description)
    {
        $customer = Customer::create(array(
            "description" => $description,
            "source" => $token,
            "email" => $email
        ));
        return $customer;
    }



    /**
     * Get stripe customer cards
     *
     * @param $customerId
     * @param $limit
     * @return array|\Stripe\StripeObject
     */
    public static function getCards($customerId, $limit)
    {
        $cards = Customer::retrieve($customerId)->sources->all(array(
            'object' => 'card', 'limit' => $limit));
        return $cards;
    }

    public static function createOrder($sku)
    {
        $items = [
            [
            "type" => "sku",
            "parent" => $sku->id],
        ];
        $order = Order::create(array(

            "items" => $items,
            "customer" => auth()->user()->fullName,
            "currency" => "usd",
            "metadata" => array("delivery_date" => Carbon::now()->format('Y-m-d')),

        ));
        echo '<pre>';
        var_dump($order);die;
    }

    /**
     * @param $product
     * @return \Stripe\ApiOperations\ApiResource
     */

    public static function createProduct($product)
    {
        $create = Product::create(array(
            "name" => $product->name,
            "description" => $product->description,
            "metadata" => array("local_id" => $product->id)

        ));
        return $create;

    }

    public static function createSKU($products)
    {
        foreach ($products as $product){
            $sku = SKU::create([
                'currency' => 'usd',
                'inventory' => [
                    'type' => 'finite',
                    'quantity' => 500,
                ],
                'price' => $product->price,
                'product' => $product->prodId,
                'attributes' => [
                    'size' => 'Medium',
                    'gender' => 'Unisex',
                    'color' => 'Cyan',
                ],
            ]);

            self::createOrder($sku);
        }

    }

    /**
     * Add card
     *
     * @param $customerId
     * @param $token
     * @return Customer
     */
    public static function addCard($customerId, $token)
    {
        $customer = self::getCustomer($customerId);
        /** @var Customer $card */
        $card = $customer->sources->create(array("source" => $token));
        return $card;
    }

    /**
     * Delete stripe card
     *
     * @param $customerId
     * @param $cardId
     * @return array
     */
    public static function deleteCard($customerId, $cardId)
    {
        $customer = Customer::retrieve($customerId);
        /** @var Customer $card */
        $card = $customer->sources->retrieve($cardId);
        $card->delete();
        return [];
    }

    /**
     * Charge
     *
     * @param $amount
     * @param $currency
     * @param $source
     * @param $description
     * @return Charge
     */
    public static function charge($amount, $currency, $source, $description)
    {
        $charge = Charge::create(array(
            "amount" => $amount,
            "currency" => $currency,
            "source" => $source,
            "description" => $description
        ));
        return $charge;
    }

    /**
     * Refund
     *
     * @param $amount
     * @param $chargeId
     * @return Refund
     */
    public static function refund($amount, $chargeId)
    {
        $refund = Refund::create(array(
            "charge" => $chargeId,
            "amount" => $amount,
            "reason" => "fraudulent"
        ));
        return $refund;
    }

    /**
     * Subscribe
     *
     * @param $customer
     * @param $planId
     * @return Subscription
     */
    public static function subscribe($customer, $planId)
    {
        $subscribe = Subscription::create(array(
            "customer" => $customer,
            "items" => array(
                array(
                    "plan" => $planId,
                ),
            )
        ));
        return $subscribe;
    }

    /**
     * Get subscription
     *
     * @param $subscriptionId
     * @return Subscription
     */
    public static function getSubscription($subscriptionId)
    {
        $subscription = Subscription::retrieve($subscriptionId);
        return $subscription;
    }

    /**
     * Cancel subscription
     *
     * @param $subscriptionId
     * @return Subscription
     */
    public static function cancelSubscription($subscriptionId)
    {
        $subscription = self::getSubscription($subscriptionId);
        if ($subscription) {
            $subscription->cancel();
        }
        return $subscription;
    }

    /**
     * @param $country
     * @param $type
     * @return \Stripe\ApiOperations\ApiResource
     */

    public static function createAccount($country,$type){
        $acct = Account::create(array(
            "country" => $country,
            "type" => $type
        ));

        return $acct;
    }

    /**
     * @param $accountId
     * @return Account
     */
    public static function retrieveAccount($accountId){
        $account = Account::retrieve($accountId);
        return $account;
    }

    /**
     * @param $money
     * @param $currency
     * @param $customerId
     * @param $accountId
     * @param $metadata
     * @return \Stripe\ApiOperations\ApiResource
     */
    public static function transfer($money,$currency,$customerId,$accountId,$metadata)
    {
        $charge = Charge::create(array(
            "amount"   => $money['owner'] *100,
            "currency" => $currency,
            "customer" => $customerId,
            "metadata" => $metadata,
                "destination" => array(
                    "amount" => $money['servant'] *100,
                    "account" => $accountId,
                ),
            )
        );

        return $charge;
    }
}