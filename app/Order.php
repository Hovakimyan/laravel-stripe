<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  integer servantId
 * @property  integer customerId
 * @property  float total
 * @property  float discount
 * @property  string comment
 * @property  string chargeId
 * @property  boolean status
 */
class Order extends Model
{
    protected $table = 'orders';

    protected  $fillable = ['chargeId'];

    /**
     * @param $orderType
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getOrders($orderType)
    {
        $result = self::query()
            ->where($orderType,auth()->user()->id)
            ->get();

        return $result;
    }

    /**
     * @param $servantId
     * @param $customerId
     * @param $total
     * @param $discount
     * @param $comment
     * @return bool|mixed
     */
    public static function saveOrder($servantId,$customerId,$total,$discount,$comment)
    {
        $order = new self();
        $order->servantId = $servantId;
        $order->customerId = $customerId;
        $order->total = $total;
        $order->discount = $discount;
        $order->comment = $comment;
        if ($order->save()){
            return $order->id;
        }
       return false;
    }

    /**
     * @param $orderId
     * @param $role
     * @param $userId
     * @return Model|null|object|static
     */
    public static function getOrder($orderId,$role,$userId)
    {
        $result = self::query()
            ->where($role.'Id',$userId)
            ->where('id',$orderId)
            ->first();
        return $result;
    }

    /**
     * @param $chargeId
     * @return int
     */
    public static function updateOrderByChargeId($chargeId)
    {
        $result = self::query()
            ->where('chargeId',$chargeId)
            ->update(['status' => 1]);

        return $result;
    }
}
