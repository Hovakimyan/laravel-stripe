<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Discount extends Model
{
    protected $table = 'discounts';

    static public function isValidCoupon($code){
        $today = Carbon::now()->format('Y-m-d');
        $result = self::where('code','=',$code)
            ->where('expirationFrom','<=',$today)
            ->where('expirationTo','>=',$today)
            ->where('maxUsage','>',DB::raw('used'))
            ->first();

        return $result;

    }

    static public function IncrementUsage($coupon){
        $result = self::whereCode($coupon)
            ->first();

        $result->used = $result->used + 1;
        $result->save();
        return;
    }

    public static function getCouponType($coupon)
    {
        $type = 'percent';
        if($coupon->discountPrice){
            $type = 'price';
        }

        return $type;
    }
}
