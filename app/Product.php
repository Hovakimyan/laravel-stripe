<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name','price','description','prodId'];

    /**
     * @param $products
     * @return mixed
     */
    public static function getOrderTotal($products,$discount = false)
    {
        $result = self::query()
            ->whereIn('id',$products)
            ->sum('price');

        if ($discount){
            $type = Discount::getCouponType($discount);
            if ($type == 'percent'){
                $result = $result - ($result * $discount->discountPercent)/100;
            }else{
                $result = $result - $discount->discountPrice;
                if ($result < 0){
                    $result = 0;
                }
            }

        }

        return $result;
    }
}
