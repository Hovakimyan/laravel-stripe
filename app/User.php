<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_CUSTOMER = 'customer';
    const ROLE_SERVANT = 'servant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullName','phoneNumber', 'email', 'password', 'registrationToken', 'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function stripeAccount(){
        return $this->hasOne('App\StripeAccount','userId','id');
    }
}
