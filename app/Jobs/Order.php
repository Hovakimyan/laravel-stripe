<?php

namespace App\Jobs;

use App\Http\Controllers\OrderController;
use App\Http\Helpers\StripeHelper;
use App\Order as OrderModel;
use App\StripeAccount;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Stripe;

class Order implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data;

    /**
     * Order constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Stripe::setApiKey('sk_test_3DHZ4mnwhVOZDAxhNIUreVOb');
        $money = OrderController::calculateAmount($this->data['amount']);

        $discount = 0;
        $randomServant = StripeAccount::getRandomServant();
        $customer=StripeAccount::getByUserId(auth()->user()->id);
        $saveOrder = OrderModel::saveOrder($randomServant->id,auth()->user()->id,$this->data['amount'],$discount,$this->data['comment']);
        $metadata = [
            'orderId' => $saveOrder,
            'servantName' => $randomServant->user->fullName,
            'customerName' => auth()->user()->fullName,
        ];

        $transfer = StripeHelper::transfer($money,'usd',$customer->customerId,$randomServant->accountId,$metadata);
        if ($transfer){
            OrderModel::find($saveOrder)->update(['chargeId'=> $transfer->id]);
        }

    }
}
