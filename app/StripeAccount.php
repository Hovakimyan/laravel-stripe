<?php

namespace App;

use App\Http\Helpers\StripeHelper;
use Illuminate\Database\Eloquent\Model;
use Stripe\Customer;
use Stripe\Stripe;

/**
 * @property  integer userId
 * @property  integer customerId
 * @property mixed accountId
 */
class StripeAccount extends Model
{
    protected $table = 'stripe_accounts';

    const STRIPE_SECRET_KEY = 'sk_test_3DHZ4mnwhVOZDAxhNIUreVOb';

    /**
     * Get by user id
     *
     * @param $userId
     * @return mixed
     */
    public static function getByUserId($userId)
    {
        return self::where('userId',$userId)->first();
    }


    /**
     * @param $email
     * @param $userId
     * @return bool
     */

    public static function createStripeCustomer($email, $userId)
    {
        Stripe::setApiKey(self::STRIPE_SECRET_KEY);
        try{
            $customer = Customer::create(array(
                "description" => "Customer for Test Laravel Project",
                "email" => $email
            ));
            $stripeAccount = new self();
            $stripeAccount->userId = $userId;
            $stripeAccount->customerId = $customer->id;
            $stripeAccount->save();
            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @param $userId
     * @param $country
     * @param $type
     * @return bool
     */

    public static function createAccount($userId, $country, $type)
    {
        Stripe::setApiKey(self::STRIPE_SECRET_KEY);
        try {

            $createAccount = StripeHelper::createAccount($country,$type);
            $stripeAccount = new self();
            $stripeAccount->userId = $userId;
            $stripeAccount->accountId = $createAccount->id;
            $stripeAccount->save();
            return true;
        }catch (\Exception $e){
            print_r($e->getMessage());
        }
    }

    /**
     * @return Model|null|object|static
     */
    public static function getRandomServant()
    {
        $result = self::query()
            ->whereNotNull('accountId')
            ->inRandomOrder()
            ->first();

        return $result;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id','userId');
    }
}
