<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>'guest'],function(){

    Route::get('/login','AuthController@login')->name('login');
    Route::post('/login','AuthController@login');
    Route::get('/verifyAccount','AuthController@verifyAccount');
    Route::get('/register','AuthController@register');
    Route::post('/register','AuthController@register');
});
Route::group(['middleware'=>'auth'],function(){
    Route::get('/home','HomeController@index');
    Route::get('/orders','OrderController@orders');
    Route::post('/orders','OrderController@orders');
    Route::get('/order/{id}','OrderController@get');
    Route::get('/logout','AuthController@logout');
    Route::post('/addCard','HomeController@addCard');
    Route::post('/deleteCard','HomeController@deleteCard');
    Route::get('/validate-discount','OrderController@validateDiscount');

});
Route::get('mail','HomeController@sendMail');
Route::any('/webhook','OrderController@webhook');
Route::any('/getPrices','OrderController@getPrices');
