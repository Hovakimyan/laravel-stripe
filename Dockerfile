FROM busybox

MAINTAINER "Dmitry Momot" <mail@dmomot.com>

RUN rm -Rvf /data/
COPY ./ /data/www
COPY ./.env.dev /data/www/.env

RUN mkdir -p /data/logs
RUN chmod -Rvf 777 /data/www/storage/logs
RUN ln -s /data/www/resources/apidoc /data/www/public/apidoc

WORKDIR /var/www
