@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Dashboard <a style="float: right;" href="/orders" class="btn btn-primary">Orders</a></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <?php if(!empty($customer)){ ?>
                        <div class="row">
                            <div class="col-sm-3">Brand</div>
                            <div class="col-sm-3">Exp Date</div>
                            <div class="col-sm-3">Last 4</div>
                            <div class="col-sm-3">Action</div>
                        </div>
                        <?php foreach ($customer['data'] as $card){ ?>
                        <div class="row">
                            <div class="col-sm-3">{{$card->brand}}</div>
                            <div class="col-sm-3">{{$card->exp_month}}/{{$card->exp_year}}</div>
                            <div class="col-sm-3">{{$card->last4}}</div>
                            <div class="col-sm-3"><a class="delete-button" style="background: white;color: red"
                                                     data-id="{{$card->id}}" href="#">Delete</a></div>
                        </div>
                        <?php }
                        }?>
                        <hr>
                        <form action="/charge" method="post" id="payment-form">
                            <div class="form-row">
                                <label for="card-element">
                                    Credit or debit card
                                </label>
                                <div id="card-element" style="width: 100%;">
                                    <!-- a Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display form errors -->
                                <div id="card-errors" role="alert"></div>
                            </div>

                            <hr>
                            <button class="btn" style="background: #32325d; color: white">Add Card</button>
                        </form>
                        <span class="error"
                              style="color:red;display: none">Something went wrong..Please try again later</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
