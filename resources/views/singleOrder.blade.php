@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">



                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Discount</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Comment</th>

                                </tr>
                                </thead>
                                <tbody>

                                        <tr>
                                            <th scope="row">{{$order->id}}</th>
                                            <td>Product</td>
                                            <td>{{$order->discount}}</td>
                                            <td>{{$order->total}}</td>
                                            <td>{{$order->comment}}</td>

                                        </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
