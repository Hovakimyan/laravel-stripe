@extends('layouts.app')


@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card card-default">
                    <div class="card-header">Dashboard   <a style="float: right;" href="/home" class="btn btn-primary">Home</a></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (auth()->user()->role == 'customer')

                            <form action="/orders" method="post">
                                @csrf

                                <div class="row">
                                    <label for="card-element">
                                        Create new order
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="products[]" id="products" class="form-control" multiple>
                                            @if ($products)
                                                @foreach ($products as $product)
                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="error-message amount-error">{{$errors->first('products')}}</span>
                                    </div>
                                    <div class="col-md-2" >
                                        <button type="submit" class="btn btn-success">Create Order</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" cols="10"  class="form-control">

                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 coupon-input ">
                                        <label for="description">Discount code</label>
                                        <input type="text" name="discount" class="form-control">
                                        <span class="discount"></span>
                                    </div>
                                    <div class="col-lg-2" style="margin-top: 30px">
                                        <button type="button" class="btn btn-primary validate-discount">Check discount code</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <p style="margin-top: 20px">Total: $<span class="total">0</span> </p>
                                    </div>

                                </div>

                            </form>
                            <hr>
                        @endif

                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Discount</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($orders)
                                    @foreach($orders as $order)
                                        <tr>
                                            <th scope="row">{{$order->id}}</th>
                                            <td>Product</td>
                                            <td>{{$order->discount}}</td>
                                            <td>{{$order->total}}</td>
                                            <td>{{$order->comment}}</td>
                                            <td><a class="btn btn-primary" href="/order/{{$order->id}}">Order Info</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
